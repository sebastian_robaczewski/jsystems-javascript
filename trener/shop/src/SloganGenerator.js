export class SloganGenerator {

    sloganMap = {
        'nalesniki': 'Najlepsze nalesniki',
        'placki': 'Pyszne placki',
        'ciastka': 'Pyszne ciastka',
        'dynamiczny': function (product) { return 'Pyszne ciastka'; }
    };

    generateSlogan(product) {
        let slogan = this.sloganMap[product.category];

        if (typeof slogan == 'function') {
            return slogan(product);
        }

        return slogan;
    }
}
