export class ProductListItem {
    constructor(product, extra) {
        this.product = product;
        this.extra = extra;
    }
    getText() {
        const { slogan = '', count, prices } = this.extra;
        const price = (prices.brutto / 100).toFixed(2) + prices.currency;
        const extra = (this.product.promoted ? "[PROMOCJA " + this.product.discount * 100 + "%]" : "");

        return `${count} ${this.product.name} - ${price} ${extra} ${slogan}`;
    }
    render() {
        const div = document.createElement('div');
        div.className = 'list-group-item';
        div.innerText = this.getText();
        return div;
    }
}
export class ProductListItemHoliday extends ProductListItem {
    getText() {
        const { slogan = '', count, prices } = this.extra;
        const price = (prices.brutto / 100).toFixed(2) + prices.currency;
        const extra = (this.product.promoted ? "[PROMOCJA SWIATECZNA " + this.product.discount * 100 + "%]" : "");

        return `${count} ${this.product.name} - ${price} ${extra} ${slogan}`;
    }
}
