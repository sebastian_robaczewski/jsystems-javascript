

// TODO:
// "fix" collapsible and button
// when btn clicked add 'show' class to .collapse element

// Extra:
// https://getbootstrap.com/docs/5.1/components/accordion/

const moje_menu = document.getElementById('moje_menu')

moje_menu.addEventListener('click', function (event) {
  const target = event.target.closest('.list-group-item')
  
  console.log(target.dataset.id)

  this.querySelectorAll('.list-group-item')
    .forEach(item => {
      item.classList.toggle('active', item === target)
    })
})
addMenuItem()
addMenuItem()

function addMenuItem() {
  const div = document.createElement('div')
  div.classList.add('list-group-item')
  div.innerHTML = 'Nowy item'
  moje_menu.append(div)
}


const btns = document.querySelectorAll('[data-bs-toggle="collapse"]')

for (let btn of btns) {
  btn.addEventListener('click', function (event) {
    // event.target === btn
    // btn.getAttribute('data-bs-target')
    const collapsibleSelector = btn.dataset.bsTarget
    document.querySelector(collapsibleSelector).classList.toggle('show')
  })
}

let counter = 3;

addSection()
addSection()

function addSection() {
  const div = document.createElement('div')
  div.innerHTML = (`
  <p>
    <button
      id="buttonB"
      class="btn btn-primary"
      type="button"
      data-bs-toggle="collapse"
      data-bs-target="#collapseExample${++counter}"
      aria-expanded="false"
      aria-controls="collapseExample${counter}"
    >
      Toggle ${counter}
    </button>
  </p>

  <div class="collapse" id="collapseExample${counter}">
    <div class="card card-body">
      Some placeholder content for the collapse component. This
      panel is hidden by default but revealed when the user
      activates the relevant trigger.
    </div>
  </div>
`)
  document.body.append(div)
}

// function Collapsible(trigger, collapsible) {
//   const button = document.querySelector(trigger)
//   const collapse = document.querySelector(collapsible)

//   button.onclick = function () {
//     collapse.classList.toggle('show')
//   }
// }
// // A
// Collapsible('#buttonA','#collapseExample')

// // B
// Collapsible('#buttonB','#collapseExample2')