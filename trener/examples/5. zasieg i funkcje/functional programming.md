

https://randycoulman.com/blog/2016/05/24/thinking-in-ramda-getting-started/
https://ramdajs.com/

http://learnyouahaskell.com/


https://static.googleusercontent.com/media/research.google.com/en//archive/mapreduce-osdi04.pdf

```js

lista = [1,2,3,4,5]

isEven = (item) => { return item % 2 === 0 }
isEven = (item) => item % 2 === 0 
isEven = item => item % 2 === 0 
// isOdd = function(item) { return item % 2 !== 0 }

// isNot = function( fn ) {
//     return function(x){
//         return !fn(x)
//     }
// }
isNot = fn => x => !fn(x) 

// isOdd = isNot( isEven )

// function compose(f , g ){ return function(x){ f(g(x)) }
not = x => !x;
pipe = (f,g) => x => f(g(x))
isOdd = pipe(not, isEven)

lista.filter( isOdd )

```