```js

var mojGlobal = 'global'

debugger;

// Anonymous Not Hoisted
var globalFn = function(){
    console.log(mojGlobal)
    var moj = 'moj'
    //     console.log(mojOuter, mojInner) // error

    return outerFn() + mojGlobal

    // Named Function - Hoisted
    function outerFn(){
        var mojOuter = 'outer'
        console.log(moj,mojGlobal, mojOuter)

        return innerFn() + mojOuter
    //     console.log(mojInner) // error

        function innerFn(){
            var mojInner = 'inner'
            console.log(mojGlobal, mojOuter, mojInner)
            return mojInner
        }
    }
}

var result = globalFn()


```

## Lazy binding closed vars
```js

var zmienna = 1;

function domykam(){
    console.log(zmienna)
    zmienna = 3 // local var
}
domykam() 
// VM53394:4 1

zmienna = 2 

domykam() 
// VM53394:4 2

```