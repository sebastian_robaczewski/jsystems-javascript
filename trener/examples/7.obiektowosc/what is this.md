```js
function WhatIsThis(){
    console.log(this) 
}
// undefined
WhatIsThis() 
// VM72773:2 Window {window: Window, self: Window, document: document, name: 'Bob', location: Location, …}

window.WhatIsThis == WhatIsThis
// true

var obj = { name:'Moj obiekt', funkcja: WhatIsThis }
// undefined

obj.funkcja()
// VM72773:2 {name: 'Moj obiekt', funkcja: ƒ}

obj.funkcja == WhatIsThis
// true
naZewnatrz = obj.funkcja
// ƒ WhatIsThis(){

naZewnatrz() 
// VM72773:2 Window

```

## Binding This

```js
WhatIsThis.call({x:1})
// {x: 1}

WhatIsThis.apply({x:2})
// {x: 2}

boundThis = WhatIsThis.bind({x:3})
boundThis()
// {x: 3}

window.name 
// 'Bob'
function Person(){
    this.sayHello = function(){ return this.name } 
}
alice = new Person('name');
greet = alice.sayHello;
greet() == window.name
// 'Bob'

```

## Function vs arrow function (lamda)

```js
function Person(name){
    this.name = name 
//     this.sayHello = function(){ return this.name } 
//     this.sayHello = this.sayHello.bind(this)
    this.sayHello = () => { return this.name } 
}

alice = new Person('name');
greet = alice.sayHello;
greet()
// 'name'

window.name 
// 'Bob'

greet.apply()
// 'name'

alice.sayHello.call({})
// 'name'

new greet()
// VM74783:1 Uncaught TypeError: greet is not a constructor
```