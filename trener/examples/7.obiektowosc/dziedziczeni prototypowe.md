```js

function Person(name){
    this.name = name 
}
Person.prototype.sayHello = function(){ return 'My name is '+this.name } 
Person.prototype.toString = Person.prototype.sayHello

alice = new Person('Alice');

function Employee(name, salary){
//     this.name = name 
    Person.apply(this,arguments)
    this.salary = salary 
}
Employee.prototype = Object.create(Person.prototype)
Employee.prototype.sayHello = function(){ 
    return `I am emploee and ${Person.prototype.sayHello.call(this)} `
}
Employee.prototype.work = function(){ return `Give me ${this.salary}` }

alice = new Person('Alice');
tom = new Employee('Tom', 2000);
// Employee
//     name: "Tom"
//     salary: 2000
//     [[Prototype]]: Employee
//         sayHello: ƒ ()
//         work: ƒ ()
//         [[Prototype]]: Person
//             sayHello: ƒ ()
//             toString: ƒ ()
//             constructor: ƒ Person(name)
//             [[Prototype]]: Object

tom.sayHello()
'I am emploee and My name is Tom '

```