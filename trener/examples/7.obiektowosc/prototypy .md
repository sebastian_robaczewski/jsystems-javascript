## Reusing function as method

```js
function Person(name){
    this.name = name 
    this.sayHello = function(){ return this.name } 
}

alice = new Person('Alice');
bob = new Person('Bob');

alice.sayHello == bob.sayHello 
false

// vs //

function Person(name){
    this.name = name 
    this.sayHello = sayHello
}
function sayHello(){ return this.name } 

alice = new Person('Alice');
bob = new Person('Bob');

alice.sayHello == bob.sayHello 
true

/* vs */

Person = (function(){
    function Person(name){
        this.name = name 
        this.sayHello = sayHello
    }
    function sayHello(){ return this.name } 
    
    return Person
})()

alice = new Person('Alice');
bob = new Person('Bob');

alice.sayHello == bob.sayHello 
true

```
## Prototype

```js
function Person(name){
    this.name = name 
}
Person.prototype.sayHello = function(){ return this.name } 

alice = new Person('Alice');
bob = new Person('Bob');

alice.sayHello == bob.sayHello 
// true
alice 
// Person 
//     name: "Alice"
//     [[Prototype]]:
//         sayHello: ƒ ()
//         constructor: ƒ Person(name)
//         [[Prototype]]: Object

window.sayHello 
// undefined
alice.__proto__.toString = alice.__proto__.sayHello 
alice.toString()
// 'Alice'
alice 
// Person {name: 'Alice'}
bob = new Person('Bob')
bob.toString()
// 'Bob'

```

## Instanceof
```js
alice instanceof Person
// true

alice instanceof Object
// true
```

