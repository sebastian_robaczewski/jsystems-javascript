```js

var product = {
    id: '',
    name: "",
    description: "",
    price: 0,
    promotion: false,
    reviews: [{ rate: 5 }]
}

// product1 = {};
// for(var klucz in product){
//     product1[klucz] = product[klucz]
// }

// Shallow copy
product1 = Object.assign({}, product)

// product1.reviews = product.reviews.slice(0,product.reviews.length)
product1.reviews = product.reviews.slice()

// product1.reviews = [{ rating: 5 }]

product1.name = 'Jabłka'
```