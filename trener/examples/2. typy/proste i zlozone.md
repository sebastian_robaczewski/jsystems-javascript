```js

o1 = {} 
// {}
o2 = {} 
// {}
o1 == o2 
// false
a = 'Pan Tadeusz'
// 'Pan Tadeusz'
b  = 'Pan Tadeusz'
// 'Pan Tadeusz'
a == b 
// true
o1 == o2 
// false
o3 = o1 
// {}
o3 == o1 
// true
o1.x = 123
// 123
o2.x = 234
// 234
o1 == o2 
// false
o1.x == o2.x 
// false
o2.x = 123
// 123
o1.x == o2.x 
// true

```