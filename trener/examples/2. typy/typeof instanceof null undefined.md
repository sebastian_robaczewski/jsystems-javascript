```js

typeof false
// 'boolean'
typeof 123
// 'number'
typeof '123'
// 'string'
typeof undefined
// 'undefined'
typeof null
// 'object'
o = {} 
// {}
if( typeof o == 'object') {} 
// undefined
o = null 
// null
if( typeof o == 'object') {} 
// undefined
null == undefined
// true
null === undefined
// false
123 === '123'
// false
typeof []
// 'object'
typeof {}
// 'object'
typeof new Date()
// 'object'
[] instanceof Array
// true
[] instanceof Object
// true

```