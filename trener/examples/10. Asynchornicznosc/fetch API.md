# GET

```js
fetch("dane.json")
  .then((res) => res.json())
  .then((data) => {
    console.log(data);
  });
```

# POST

```js
fetch("/todos", {
  method: "POST",
  headers: { "Content-Type": "application/json" },
  body: JSON.stringify({
    title: data.title,
    completed: false,
    userId: 1,
  }),
})
  .then((res) => res.json())
  .then((data) => {
    console.log(data);
  });
```

## REST server

https://github.com/typicode/json-server

npx json-server ./db.json --watch -s ./

## CORS

Access-Control-Allow-Credentials: true
Access-Control-Allow-Origin: http://localhost:3000
Access-Control-Expose-Headers: Location
