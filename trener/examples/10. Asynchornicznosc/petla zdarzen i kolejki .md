```js
document.querySelector('input').oninput = event => console.log(event.target.value) 

console.log(1)

setTimeout(function(){
    console.log(2)
},0)

Promise.resolve(3).then(console.log)

console.log(4)


start = Date.now()
while(start + 5_000 > Date.now()){}

console.log(5)

1
4
// ... 5 sec ... 
5 
// finished sync work
3  // Async - Microtasks queue - Promise

// Async -  User events
a
al
ala
alam

// Async -  Macro Tasks - Timeout, Interval, Fetch/XHR, HTML5 APS.. 
2
```