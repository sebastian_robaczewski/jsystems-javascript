
## Selectors

```js
document.getElementsByClassName('table')
// HTMLCollection [table#tabela_raty.table.table-striped, tabela_raty: table#tabela_raty.table.table-striped]

tabela_raty
// <table class=​"table table-striped" id=​"tabela_raty">​…​</table>​

var tabela_raty = 123
tabela_raty
// 123

document.getElementById('tabela_raty')
// <table class=​"table table-striped" id=​"tabela_raty">​…​</table>​

document.querySelector('#tabela_raty > tbody')
// <tbody>​…​</tbody>​

document.querySelectorAll('#tabela_raty tr')
// NodeList [tr]

const tabelaElem = document.getElementsByClassName('table')
const tabelaBody = tabelaElem.querySelector('tbody')
```