class Product {
    constructor(name, description, price, discount, promoted) {
        this.id = Product.incrementId();
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.promoted = promoted;
    }

    static incrementId() {
        if (!this.lastId) this.lastId = 1;
        else this.lastId++;
        return this.lastId;
    }

    getPrice() {
        return ((this.price * ((100 - this.discount) / 100)) * (1 + tax / 100)).toFixed(2);
    }

    getPromoted() {
        return (this.promoted == true ? " [Promocja -" + this.discount + "%]" : "");
    }

    addSpecialDescription(productName) {
        return "Zapraszamy na " + sloganMap[productName] || '';
    }

    createItemDIV(product) {
        let productListEl = document.createElement('div');
        productListEl.className = "list-group-item";
        productListEl.innerText = product;
        return productListEl;
    }
}

class ProductsListComponent {
    productsList = document.getElementById("productsList");

    constructor(products) {
        this.products = products;
    }

    render() {
        this.cleanProductsList();

        for (let i = 0; i < this.products.length; i++) {
            let realIndex = this.checkDirection(this.direction, i);

            if (this.products[realIndex].promoted === this.isPromoted || this.isPromoted == undefined) {
                if (i < this.limit) {
                    var product = this.getFullProduct(realIndex);
                    productsList.append(this.products[realIndex].createItemDIV(product));
                }
            }
        }
    }

    checkDirection(direction, currentIndex) {
        if (direction == -1)
            return this.products.length - currentIndex - 1;
        else
            return currentIndex;
    }

    getFullProduct(realIndex) {
        return `${products[realIndex].addSpecialDescription(this.products[realIndex].name)} ${this.products[realIndex].description} - ${products[realIndex].getPrice()}zł ${products[realIndex].getPromoted()}`;
    }

    cleanProductsList() {
        productsList.innerHTML = '';
    }
}

const tax = 23;
const products = [
    new Product("Placki", "Pani Wiesi", 100, 10, true),
    new Product("Naleśniki", "prosto z patelni", 60, 0, false),
    new Product("Pączki", "w każdym smaku", 40, 20, true),
    new Product("Placki", "Pani Wiesi", 100, 10, true),
    new Product("Naleśniki", "prosto z patelni", 60, 0, false),
    new Product("Pączki", "w każdym smaku", 40, 20, true),
]

const sloganMap = {
    'Placki': 'najlepsze placki',
    'Naleśniki': 'pyszne naleśniki',
    'Pączki': 'swieże pączki',
}

// -------------------------------

const productsListComponent = new ProductsListComponent(products);
productsListComponent.isPromoted = undefined;
productsListComponent.limit = 3;
productsListComponent.direction = -1;
productsListComponent.render();
