const tableEl = document.getElementById("tabela_raty");
const tBodyEl = tableEl.querySelector('tbody');
const tFootEl = tableEl.querySelector('tfoot');
const formEl = document.querySelector('form');

const inputEls = document.querySelectorAll('input');
inputEls.forEach(item => {
    item.addEventListener('input', (event) => {
        checkRange(event);
        recalculate();
    });
    item.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            recalculate();
            event.preventDefault();
        }
    });
});

function initData() {
    document.getElementById("amount").value = 1000;
    document.getElementById("interestRate").value = 2;
    document.getElementById("duration").value = 12;
    recalculate();
}

function checkRange(event) {
    let target = event.target;

    if (target.type == "range")
        document.getElementById(target.id).parentElement.querySelector('span').innerHTML = target.value;
}

// --------------------------------------

function formatCurrency(cents) {
    return parseInt(cents).toFixed(2) + "zł";
}



function recalculate() {
    clearTable();

    let amount = 0, interestRate = 0, duration = 0;

    inputEls.forEach(inputEl => {
        switch (inputEl.id) {
            case 'amount':
                amount = inputEl.value;
                break;

            case 'interestRate':
                interestRate = inputEl.value;
                break;

            case 'duration':
                duration = inputEl.value;
                break;
        }
    });

    updateTable(amount, interestRate, duration);
}


function clearTable() {
    tBodyEl.innerHTML = '';
    tFootEl.innerHTML = '';
}


function updateTable(amount, interestRate, duration) {
    clearTable()
    const rows = []
    const totals = {
        payments: 0,
        capital: 0,
        interest: 0,
    }
    let remining = amount;
    for (let i = 0; i < duration; i++) {
        const capital = amount / duration;
        const interest = remining * interestRate / 100 / duration
        const payment = capital + interest
        const row = {
            period: i + 1,
            remining,
            payment,
            capital,
            interest
        }
        rows.push(row)
        totals.payments += payment
        totals.capital += capital
        totals.interest += interest
        remining -= capital
        generateRow(row);
    }
    generateFooterRow(totals)
    return { rows, totals }
}

function generateRow(row) {
    let rowEl = document.createElement('tr');

    rowEl.innerHTML = /* html */`
        <td>${row.period}</td>
        <td>${formatCurrency(row.remining)}</td>
        <td>${formatCurrency(row.payment)}</td>
        <td>${formatCurrency(row.capital)}</td>
        <td>${formatCurrency(row.interest)}</td>
        `;

    tBodyEl.append(rowEl);
}

function generateFooterRow(totals) {
    let rowEl = document.createElement('tr');
    rowEl.innerHTML = /* html */`
        <th colspan="2">Suma:</th>
        <th>${formatCurrency(totals.payments)}</th>
        <th>${formatCurrency(totals.capital)}</th>
        <th>${formatCurrency(totals.interest)}</th>
        `;

    tFootEl.append(rowEl);
}

const { rows, totals } = updateTable();

initData();