var tax = 23;
var products = [
    { id: '1', name: 'Placki', description: "Pani Wiesi", price: 100, discount: 10, promoted: true },
    { id: '2', name: 'Naleśniki', description: "prosto z patelni", price: 56.50, discount: 0, promoted: false },
    { id: '3', name: 'Pączki', description: "w każdym smaku", price: 30.50, discount: 20, promoted: true },
]
var isPromoted = undefined;
var limit = 3;
var direction = 1;

var productsList = document.getElementById("productsList");

for (let i = 0; i < products.length && i < limit; i++) {
    if (direction == -1) {
        realIndex = products.length - i - 1;
    } else realIndex = i;

    if (products[realIndex].promoted === isPromoted || isPromoted == undefined) {
        var info = "Zapraszamy na ";

        switch (products[realIndex].name) {
            case "Placki":
                info += "najlepsze placki";
                break;

            case "Naleśniki":
                info += "pyszne naleśniki";
                break;

            case "Pączki":
                info += "swieże pączki";
                break;

            default:
                break;
        }

        var price = ((products[realIndex].price * ((100 - products[realIndex].discount) / 100)) * (1 + tax / 100)).toFixed(2);
        var promoted = (products[realIndex].promoted == true ? " [Promocja -" + products[realIndex].discount + "%]" : "");

        var productListEl = document.createElement('div');
        productListEl.className = "list-group-item";
        productListEl.innerText = `${info} ${products[realIndex].description} - ${price}zł ${promoted}`;

        productsList.append(productListEl);
    }
}


