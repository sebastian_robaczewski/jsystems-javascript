// Produkt:
// - id
// - name
// - description
// - price
// - promotion

var reviews = [{ rating: 5 }];

product1 = {
    id: '1',
    name: "jablko",
    description: "jablko desc",
    price: 1.4,
    promotion: true
};

product2 = {
    id: '2',
    name: "gruszka",
    description: "gruszka desc",
    price: 2.2,
    promotion: true
};

product3 = {
    id: '3',
    name: "banan",
    description: "banan desc",
    price: 4.4,
    promotion: false
};

product1.reviews = reviews;
product2.reviews = reviews;
product3.reviews = reviews;

console.log(product1);
console.log(product2);
console.log(product3);

product1.reviews == product2.reviews;


product = {
    id: '',
    name: "",
    description: "",
    price: '',
    promotion: false,
    reviews: [{ rating: 5 }]
};

prod1 = Object.assign({}, product);
prod1.reviews = product.reviews.slice();
prod1.name = "jablka";

console.log(prod1);