const products = [
    {
        name: 'Product A',
        price: 100,
    },
    {
        name: 'Product B',
        price: 320,
    },
    {
        name: 'Product C',
        price: 320,
    },
    {
        name: 'Product D',
        price: 320,
    }
];

const cartData = {
    items: [
        {
            name: 'Product ABC',
            price: 100,
            amount: 2,
            subtotal: 200,
        }
    ],
    total: 0
};


function addToCart(product) {
    var productInCart = findProduct(product.name);

    if (productInCart === undefined) {
        product.amount = 1;
        product.subtotal = product.price;
        cartData.items.push(product);
    } else {
        var index = cartData.items.indexOf(productInCart);
        productInCart.amount++;
        productInCart.subtotal = productInCart.amount * product.price;
        cartData.items[index] = productInCart;
    }
}

function removeFromCart(product) {
    var productInCart = findProduct(product.name);

    if (productInCart === undefined) {
        console.log("brak danego produktu");
    } else {
        var index = cartData.items.indexOf(productInCart);
        productInCart.amount--;
        productInCart.subtotal = productInCart.amount * product.price;
        if (productInCart.amount == 0)
            cartData.items.splice(index, 1);
        else
            cartData.items[index] = productInCart;
    }
}

function getCartTotal() {
    let total = 0;

    cartData.items.forEach(cartEl => {
        total += cartEl.subtotal;
    });

    cartData.total = total

    return cartData.total;
}

function findProduct(name){
    return cartData.items.find(function (item) {
        return item.name == name;
    });
}

addToCart(products[0])
console.log(cartData, getCartTotal());

addToCart(products[1])
console.log(cartData, getCartTotal());

addToCart(products[1])
console.log(cartData, getCartTotal());

addToCart(products[1])
console.log(cartData, getCartTotal());

removeFromCart(products[1])
console.log(cartData, getCartTotal());