var productsList = document.getElementById("productsList");

const tax = 23;
const products = [
    { id: '1', name: 'Placki', description: "Pani Wiesi", price: 100, discount: 10, promoted: true },
    { id: '2', name: 'Naleśniki', description: "prosto z patelni", price: 56.50, discount: 0, promoted: false },
    { id: '3', name: 'Pączki', description: "w każdym smaku", price: 30.50, discount: 20, promoted: true },
]

const sloganMap = {
    'Placki': 'najlepsze placki',
    'Naleśniki': 'pyszne naleśniki',
    'Pączki': 'swieże pączki',
}

function showProducts(isPromoted, limit, direction) {
    cleanProductsList();

    for (let i = 0; i < products.length && i < limit; i++) {
        let realIndex = checkDirection(direction, i);

        if (products[realIndex].promoted === isPromoted || isPromoted == undefined) {
            addProductToList(realIndex);
        }
    }

    function checkDirection(direction, currentIndex) {
        if (direction == -1)
            return products.length - currentIndex - 1;
        else
            return currentIndex;
    }

    function addProductToList(realIndex){
        let product = `${addSpecialDescription(products[realIndex].name)} ${products[realIndex].description} - ${getPrice()}zł ${getPromoted()}`;

        createItemDIv();
        logProduct();

        function createItemDIv(){
            let productListEl = document.createElement('div');
            productListEl.className = "list-group-item";
            productListEl.innerText = product;
            productsList.append(productListEl);
        }

        function getPrice() {
            return ((products[realIndex].price * ((100 - products[realIndex].discount) / 100)) * (1 + tax / 100)).toFixed(2);
        }
    
        function getPromoted() {
            return (products[realIndex].promoted == true ? " [Promocja -" + products[realIndex].discount + "%]" : "");
        }

        function addSpecialDescription(productName) {
            let info = "Zapraszamy na ";
    
            info += sloganMap[productName] || '';
    
            return info;
        }

        function logProduct() {
            console.log(product);
        }
    }

    function cleanProductsList() {
        productsList.innerHTML = '';
    }
}







