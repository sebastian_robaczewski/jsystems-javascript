var tax = 23;

class Product {
    //reviews = [{ rating: 5 }];
    constructor(name, description, price, discount, promoted) {
        this.id = Product.incrementId();
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.promoted = promoted;
    }

    static incrementId() {
        if (!this.lastId) this.lastId = 1;
        else this.lastId++;
        return this.lastId;
    }

    printProduct() {
        console.log(this.name + " - " + ((this.price * ((100-this.discount)/100)) * (1 + tax/100)).toFixed(2) + (this.promoted ? " [Promocja -" + this.discount + "%]" : ""))  
    }

    printProductOld() { 
        if (this.discount > 0)
        console.log(this.name + " - " + ((this.price * ((100-this.discount)/100)) * (1 + tax/100)).toFixed(2) + (this.promoted ? " [Promocja -" + this.discount + "%]" : ""))  
        else
        console.log(this.name + " - " + ((this.price * ((100-this.discount)/100)) * (1 + tax/100)).toFixed(2) + (this.promoted ? " [Promocja -" + this.discount + "%]" : ""))  
    }
}

const prod1 = new Product("jablko", "jablko desc", 14.2, 0.1, true);
const prod2 = new Product("gruszki", "gruszki desc", 32.6, 0.3, true);
const prod3 = new Product("banan", "banan desc", 46.9, 0, false);


prod1.printProduct();
prod2.printProduct();
prod3.printProduct();